package com.example.rest.restfulwebservice.controller.filter;

import com.example.rest.restfulwebservice.model.FilterBean;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class FilterController {

    @GetMapping("/filter")
    public MappingJacksonValue fileds() {
        FilterBean filterBean = new FilterBean("Value1", "Value2", "Value3");
        SimpleBeanPropertyFilter simpleBeanPropertyFilter = SimpleBeanPropertyFilter.filterOutAllExcept("filed1", "filed2");
        FilterProvider filterProvider = new SimpleFilterProvider().addFilter("FilterBean", simpleBeanPropertyFilter);
        MappingJacksonValue mapping = new MappingJacksonValue(filterBean);
        mapping.setFilters(filterProvider);
        return mapping;

    }

    @GetMapping("/filter-list")
    public MappingJacksonValue filedList() {
        List<FilterBean> beanList = Arrays.asList(new FilterBean("Value1", "Value2", "Value3"),
                new FilterBean("Value11", "Value22", "Value33"),
                new FilterBean("Value111", "Value222", "Value333"));

        SimpleBeanPropertyFilter simpleBeanPropertyFilter = SimpleBeanPropertyFilter.filterOutAllExcept("filed1", "filed2","filed3");
        FilterProvider filterProvider = new SimpleFilterProvider().addFilter("FilterBean", simpleBeanPropertyFilter);
        MappingJacksonValue mapping = new MappingJacksonValue(beanList);
        mapping.setFilters(filterProvider);
        return mapping;
    }
}
