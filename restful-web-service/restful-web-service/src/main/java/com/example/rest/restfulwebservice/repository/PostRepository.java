package com.example.rest.restfulwebservice.repository;

import com.example.rest.restfulwebservice.model.Post;
import com.example.rest.restfulwebservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
}
